use std::io::stdin;
use std::io::BufRead;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug, PartialEq, Eq, Clone)]
/// A Variable isa tuple consisting of the ID and the value.
struct Variable(u32, bool);

impl From<i32> for Variable {
    fn from(val_id: i32) -> Variable {
        let value = val_id > 0;
        let id = val_id.abs();

        Variable(id as u32, value)
    }
}

impl Display for Variable {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let mut var: i32 = self.0 as i32;

        // Negative, let's negate it
        if !self.1 {
            var *= -1;
        }

        write!(f, "{}", var)
    }
}

#[derive(Debug)]
/// A Clause is a disjunctive set of variables.
struct Clause {
    variables: Vec<Variable>
}

impl Clause {
    /// Returns true if the set of variables can satisfy this clause, or false
    /// if it can't. or None if the variables are not exhausted.
    fn accept(&self, variables: &Vec<Variable>) -> Option<bool> {
        // Check if any variables satisfy thise clause
        for var in self.variables.iter() {
            if variables.iter().any(| v | v == var) {
                println!("{} satisfies {:?}", var, variables);
                return Some(true);
            }
        }

        // Check if it's possible for any variable to still satisfy this clause.
        for var in self.variables.iter().map(| v | v.0) {
            if !variables.iter().map(| v | v.0).any(| v | v == var) {
                return None;
            }
        }

        println!("Cannot satisfy {:?} with {:?}", self.variables, variables);

        Some(false)
    }
}

impl<T> From<T> for Clause where T: Iterator<Item=i32> {
    fn from(numbers: T) -> Clause {
        let variables = numbers.map(| n | Variable::from(n));
        Clause {
            variables: variables.collect()
        }
    }
}

#[derive(Debug)]
/// A Model is a conjunctive set of clauses.
struct Model {
    clauses: Vec<Clause>
}

impl Model {
    /// Returns false if the current variable set cannot satisfy the clauses
    /// or true otherwise.
    fn propagate(&self, variables: &Vec<Variable>) -> bool {
        for clause in self.clauses.iter() {
            match  clause.accept(variables) {
                Some(false) => return false,
                _ => ()
            }
        }
        true
    }

    fn satisfy_helper(&self, set: &mut Vec<Variable>, depth: usize, explored: &mut u32) -> bool {
        // Make sure the current set satisfies
        if !self.propagate(set) {
            return false;
        }

        // Pick the clause to satisfy.
        // If we have no more clauses left, we're done
        let clause_to_satisfy = match self.clauses.get(depth) {
            None => return true,
            Some(c) => c
        };

        // Skip if we already satisfy this clause
        if clause_to_satisfy.accept(set) == Some(true) {
            // Attempt to satisfy it
            if self.satisfy_helper(set, depth + 1, explored) {
                return true;
            }
        }

        // Attempt to use each variable
        for var in clause_to_satisfy.variables.iter() {
            // Get the id of each used variable
            let used_vars: Vec<_> = set.iter().map(| v | v.0).collect();

            // If we've used this variable already, skip it
            if used_vars.contains(&var.0) {
                continue
            }

            // We have not used this variable yet. Add it to the set.
            let to_use = var;
            *explored += 1;
            set.push(to_use.clone());

            // Attempt to satisfy it
            if self.satisfy_helper(set, depth + 1, explored) {
                return true;
            }

            // Could not satisfy with this variable, let's remove it and move on
            let _ = set.pop();
        }

        // We've tried all variables and failed. Let's backtrack
        println!("Backtracking {:?}", set);
        false
    }

    fn satisfy(&self) -> (Option<Vec<Variable>>, u32) {
        let mut set: Vec<Variable> = Vec::new();
        let mut explored = 0;

        if self.satisfy_helper(&mut set, 0, &mut explored) {
            (Some(set), explored)
        } else {
            (None, explored)
        }
    }
}

impl<T> From<T> for Model where T: Iterator<Item=i32> {
    fn from(numbers: T) -> Model {
        let numbers: Vec<i32> = numbers.collect();

        let clauses = numbers.split(| &n | n == 0)
            .map(| i | {
                Clause::from(i.to_vec().into_iter())
            }).filter(| v | v.variables.len() != 0);

        // Sort clauses by number of constraining variables
        let mut clauses: Vec<Clause> = clauses.collect();

        clauses.sort_by_key(| c | c.variables.len());

        Model {
            clauses: clauses
        }
    }
}

fn main() {
    // Grab the stdin lock and iterate over the lines, skipping comments
    let stdin = stdin();
    let lock = stdin.lock();
    let lines: Vec<String> = lock.lines()
        .map(| r | r.unwrap())
        .filter(| l | l.split_whitespace().next() != Some("c")).collect();

    let mut input = lines.iter().flat_map(| f | f.split_whitespace());

    // Ignore the `p` and `cnf`
    let _ = input.next();
    let _ = input.next();

    let var_count = input.next().unwrap().parse::<u32>();
    let clause_count = input.next().unwrap().parse::<u32>();

    let model = Model::from(input.map(| s | s.parse::<i32>().unwrap()));

    let (sat, exp) = model.satisfy();

    match sat {
        None => {
            println!("s cnf 0");
        },
        Some(solution) => {
            println!("s cnf 1 {} {}", var_count.unwrap(), clause_count.unwrap());
            for v in solution.iter() {
                println!("v {}", v);
            }
        }
    }

    println!("{} branching nodes explored.", exp);
}
